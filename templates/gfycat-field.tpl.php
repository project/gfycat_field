<?php
/**
 * @file
 * gfycat-field.tpl.php
 */
?>
<video class="gfyVidIframe" id="gfyVid" autoplay="" loop="" poster="//thumbs.gfycat.com/<?php print $variables['gfycat_name']; ?>-poster.jpg" style="-webkit-backface-visibility: hidden;-webkit-transform: scale(1);" width="100%">
  <source id="webmsource" src="<?php print $variables['webm_source']; ?>" type="video/webm">
  <source id="mp4source" src="<?php print $variables['mp4_source']; ?>" type="video/mp4">
  <img src="<?php print $variables['img_source']; ?>">
</video>
