# GfyCat Field

## Requirements

* [GfyCat PHP Library](https://bitbucket.org/bbcworldwide/gfycat-php)
* [Drush](https://github.com/drush-ops/drush)
* [Composer](https://www.drupal.org/docs/develop/using-composer/using-composer-with-drupal)
* [Composer Manager](https://www.drupal.org/node/2405805)

## Introduction

This module implement functionality with the GfyCat API

## Installation with Drush
* Download the modules listed under the requirements section, and composer manager: `drush en -y composer_manager`
* Composer Manager writes a file to `sites/default/files/composer`
* As long as Composer Manager is enabled, the required dependencies will be added to `sites/all/vendor` as soon as you enable the module
* If you don't see any dependencies downloaded, try `drush composer-json-rebuild` followed by `drush composer-manager install` when in the docroot
* Check `/admin/config/system/composer-manager` to ensure it's all green
* Tip: If you ever want to update your composer dependencies to a more recent version (while respecting versioning constraints) try `drush composer-manager update`

## Configuration
* Go to the GfyCat configuration screen at `/admin/config/gfycat_field` and enter your GfyCat API credentials.
* Note: If no credentials are entered, then the Anonymous API will be used.

## Current maintainers:
- Jon Bloomfield - https://www.drupal.org/u/jbloomfield
- Temi Jegede - https://www.drupal.org/u/t14
- Ian Westacott - https://www.drupal.org/u/ianwesty
