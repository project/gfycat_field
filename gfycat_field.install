<?php
/**
 * @file
 */

/**
 * Implements hook_field_schema().
 */
function gfycat_field_field_schema() {
  return array(
    'columns' => array(
      'fid' => array(
        'description' => 'The {file_managed}.fid being referenced in this field.',
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
      'alt' => array(
        'description' => "Alternative image text, for the image's 'alt' attribute.",
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ),
      'title' => array(
        'description' => "Image title text, for the image's 'title' attribute.",
        'type' => 'varchar',
        'length' => 1024,
        'not null' => FALSE,
      ),
      'width' => array(
        'description' => 'The width of the image in pixels.',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'height' => array(
        'description' => 'The height of the image in pixels.',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'gfycat_name' => array(
        'description' => 'The name returned by Gfycat to embed.',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'webm_source' => array(
        'description' => 'The webmsource returned by Gfycat to embed.',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'mp4_source' => array(
        'description' => 'The mp4source returned by Gfycat to embed.',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'img_source' => array(
        'description' => 'The imgsource returned by Gfycat to embed.',
        'type' => 'text',
        'not null' => FALSE,
      ),
    ),
    'indexes' => array(
      'fid' => array('fid'),
    ),
    'foreign keys' => array(
      'fid' => array(
        'table' => 'file_managed',
        'columns' => array('fid' => 'fid'),
      ),
    ),
  );
}

/**
 * Add the MP4 extension to existing content types with GfyCat fields.
 */
function gfycat_field_update_7000() {
  $result = db_query("select id, field_name, entity_type, bundle, data from {field_config_instance} where data LIKE '%file_extensions%' and data LIKE '%gfycat_name%' and entity_type = 'node'");
  $records = $result->fetchAll();
  if (!empty($records)) {
    foreach ($records as $record) {
      $data = unserialize($record->data);
      if ($data['settings']['file_extensions'] && array_key_exists('gfycat_name', $data['settings'])) {
        $data['settings']['file_extensions'] .= ' mp4';

        // Re-serialize the data and update the DB.
        $data = serialize($data);
        $update = db_update('field_config_instance')
          ->fields(array('data' => $data))
          ->condition('id', $record->id, '=')
          ->execute();
      }
      // db_update() should return the number of rows updated.
      if (!is_int($update)) {
        watchdog('gfycat_field_update', t('Update 7000 - Unable to add extension %extensions to %field_name for entity %entity_type %bundle', array(
          '%extensions' => 'mp4',
          '%field_name' => $record->field_name,
          '%entity_type' => $record->entity_type,
          '%bundle' => $record->bundle
        )));
      }
    }
  }
}

/**
 * Change the form widget to existing content type from image to file
 * for GfyCat fields.
 */
function gfycat_field_update_7001() {
  $entity_type = 'node';
  $widget_type_machine_name = 'file_generic';

  // Get all node types.
  $node_types = node_type_get_names();
  foreach ($node_types as $node_type_machine_name => $node_type) {
    // Get the fields info for the node type.
    $fields_info = field_info_instances($entity_type, $node_type_machine_name);

    foreach ($fields_info as $field_name => $value) {
      $field_info = field_info_field($field_name);
      if ($field_info['type'] == 'gfycat_upload') {
        // Retrieve the stored instance settings to merge with
        // the incoming values.
        $instance = field_read_instance($entity_type, $field_info['field_name'], $node_type_machine_name);

        // Set the right module information.
        $widget_type = field_info_widget_types($widget_type_machine_name);
        $widget_module = $widget_type['module'];

        $instance['widget']['type'] = $widget_type_machine_name;
        $instance['widget']['module'] = $widget_module;

        // Update the field instance with the file widget configuration.
        field_update_instance($instance);
      }
    }
  }
  field_cache_clear();
}
