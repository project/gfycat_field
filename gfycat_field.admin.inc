<?php

/**
 * @file
 * Administrative page callbacks for the GfyCat Field module.
 */

/**
 * Form constructor for the GfyCat API Config form.
 */
function gfycat_field_admin_form($form, &$form_state) {
  $form = array();

  $form['gfycat_field_description'] = array(
    '#markup' => '<p>' . t('If no credentials are entered, then the Anonymous API will be used.') . '</p>'
  );

  $form['gfycat_field_username'] = array(
    '#type' => 'textfield',
    '#title' => t('GfyCat API Username'),
    '#description' => t('Enter your GfyCat API Username'),
    '#default_value' => variable_get('gfycat_field_username', ''),
  );

  $form['gfycat_field_password'] = array(
    '#type' => 'textfield',
    '#title' => t('GfyCat API Password'),
    '#description' => t('Enter your GfyCat API Password'),
    '#default_value' => variable_get('gfycat_field_password', ''),
  );

  $form['gfycat_field_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('GfyCat API Client ID'),
    '#description' => t('Enter your GfyCat API Client ID'),
    '#default_value' => variable_get('gfycat_field_client_id', ''),
  );

  $form['gfycat_field_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('GfyCat API Client Secret'),
    '#description' => t('Enter your GfyCat API Client Secret'),
    '#default_value' => variable_get('gfycat_field_client_secret', ''),
  );

  $form['gfycat_field_grant_type'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('gfycat_field_grant_type', 'password'),
  );

  return system_settings_form($form);
}
